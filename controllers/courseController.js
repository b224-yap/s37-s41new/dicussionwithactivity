const Course = require("../models/Course");


// Controller Functions


// Creating a new course
module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description : reqBody.description,
		price : reqBody.price 
	});
	return newCourse.save().then((course,error)=>{
		if(error) {
			return false //"Course cration failed"
		}else {
			return true
		}
	})
}


// Retrievinf ALL Courses

module.exports.getAllCourses = (data) => {
	console.log(data.isAdmin)
	if(data.isAdmin) {

		return Course.find({}).then(result => {

			return result
		})
	}else {
		return false //"You are not an admin"
	}
};

// Retrieve All Active Courses

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}


// Retrieve a Specific Course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
};

// Update a Specific Course

module.exports.updateCourse = (reqParams, reqBody) =>{
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price : reqBody.price
	}

	return  Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse,error) => {
		console.log(updatedCourse)
		if(error){
			return false
		}else{
			return true
		}
	})
}

// Activity solution
module.exports.updateCourseStatus = (reqParams, reqBody) => {
	let updatedCourseStatus ={
		isActive : reqBody.isActive
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourseStatus).then((updatedCourseStatus,error) =>{
		console.log(updatedCourseStatus)
		if(error){
			return false
		}else{
			return true
		}
	})
}